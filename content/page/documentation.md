+++
title = "Documentation"
+++

The latest documentation is available online and part of the continuous integration:

 * [libaccounts-glib reference manual](http://accounts-sso.gitlab.io/libaccounts-glib/)
 * [libaccounts-qt reference manual](http://accounts-sso.gitlab.io/libaccounts-qt/)
 * [libsignon-glib reference manual](http://accounts-sso.gitlab.io/libsignon-glib/)
 * [libsignon-qt reference manual](http://accounts-sso.gitlab.io/signond/)

There is also a documentation to create daemon-side extensions and add new authentication methods:

 * [gSignon daemon documentation](http://accounts-sso.gitlab.io/gsignond/)

The plugins themselves are documented to be able to integrate them easily:

 * [OAuth 1.0 and 2.0 plugin for the gSignon daemon](http://accounts-sso.gitlab.io/gsignond-plugin-oa/)
 * [SASL plugin for the gSignon daemon](http://accounts-sso.gitlab.io/gsignond-plugin-sasl)
 * [IMAP and SMTP plugin for the gSignon daemon](https://accounts-sso.gitlab.io/gsignond-plugin-mail/gsignond-plugin-mail/index.htm)

Some language specific documentation is also available:

 * [Vala on valadoc.org](https://valadoc.org/)
