+++
title = "Accounts SSO"
+++

An Accounts and Authentication framework.

Accounts SSO is a framework for application developers who wish to acquire, use and store web account details and credentials. It handles the authentication process of an account and securely stores the credentials and service-specific settings.

This is the collection of two libraries:

 * [libaccounts-glib](https://gitlab.com/accounts-sso/libaccounts-glib) (there is a [libaccounts-qt](https://gitlab.com/accounts-sso/libaccounts-qt) binding) to manage accounts and associate services with various providers.
 * [libsignon-glib](https://gitlab.com/accounts-sso/libsignon-glib) (there is a [libsignon-qt](https://gitlab.com/accounts-sso/signond) counterpart) to manage account credentials and authenticate against services.

The authentication is handled by a daemon. At the time of writing there are two daemons available which implements the same D-Bus API:

 * [signond](https://gitlab.com/accounts-sso/signond) - written in C++ with Qt
 * [gsignond](https://gitlab.com/accounts-sso/gsignond) - written in C with GLib

Both daemons handle the same amount of services and a special care is taken to be able to use both daemons as drop-in replacement.

The Accounts SSO framework is used by:

 * [KDE](https://www.kde.org/)
 * [elementary OS](https://elementary.io/)

Need to talk to someone?

 * Mailing list: [Annoucement and Development](https://groups.google.com/d/forum/accounts-sso-devel)
 * IRC: #accounts-sso on Freenode

